package it.unibo.oop.lab.mvcio2;

import java.io.*;
import java.awt.*;
import javax.swing.*;


/**
 * 
 */
public class Controller {

    private static final String PATH = System.getProperty("user.home")
            + System.getProperty("file.separator") + "output.txt";
    private File myFile;
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class with:
     * 
     * 1) A method for setting a File as current file
     * 
     * 2) A method for getting the current File
     * 
     * 3) A method for getting the path (in form of String) of the current File
     * 
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     * 
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    public Controller() {
        this.setCurrentFile(PATH);
    }
    
    public final File setCurrentFile(final String s) {
        return this.myFile = new File(s);
    }

    public final File getCurrentFile() {
        return myFile.getAbsoluteFile();
    }
    public final String getPath() {
        return myFile.getAbsolutePath();
    }
    
    public final void writeOnFile(final String s) {
        try (PrintStream ps = new PrintStream(myFile)) {
            ps.print(s);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
    }
}
