package it.unibo.oop.lab.mvcio2;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.*;


/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */
    private final JFrame frame = new JFrame("Writing And Saving on file");
    private Controller file = new Controller();
    private static final Color background_color = new Color(255, 255, 102);
    private static final Color button_color = new Color(250, 85, 85);
    private static final Color blu_color = new Color(0, 204, 204);
    private static final Color green_color = new Color(0, 255, 0);



    public SimpleGUIWithFileChooser() {
        final JPanel myPanel = new JPanel();
        final JPanel sdPanel = new JPanel();
        final JPanel thPanel = new JPanel();
        final JTextArea ta = new JTextArea("Insert text here!");
        final JButton save = new JButton("Save");
        final JTextField tf = new JTextField(file.getPath());
        final JButton browse = new JButton("Browse...");
        tf.setEditable(false);
        tf.setBackground(blu_color);
        tf.setFont(new Font("Arial Black", Font.BOLD,15));
        ta.setBackground(background_color);
        ta.setFont(new Font("Arial Black", Font.BOLD, 12));
        save.setBackground(button_color);
        save.setFont(new Font("Arial Black", Font.BOLD, 25));
        save.setBorderPainted(true);
        browse.setBackground(green_color);
        browse.setBorderPainted(true);
        myPanel.setLayout(new BorderLayout());
        myPanel.add(ta, BorderLayout.CENTER);
        myPanel.add(save, BorderLayout.SOUTH);
        sdPanel.setLayout(new BorderLayout());
        myPanel.add(sdPanel, BorderLayout.NORTH);
        sdPanel.add(tf);
        thPanel.setLayout(new BorderLayout());
        sdPanel.add(thPanel, BorderLayout.EAST);
        thPanel.add(browse);
        browse.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
               JFileChooser j = new JFileChooser();
               int returnVal = j.showSaveDialog(browse);
             if (returnVal == JFileChooser.APPROVE_OPTION) {
                file.setCurrentFile(j.getSelectedFile().getAbsolutePath());
                tf.setText(file.getPath());
             }
             else if (returnVal == JFileChooser.CANCEL_OPTION) {
                 JOptionPane.showMessageDialog(myPanel, "ERROR: NO FILE SELECTED!\nDEFAULT: " + file.getPath());
              }
            }
        });
        save.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                file.writeOnFile(ta.getText());
                JOptionPane.showMessageDialog(myPanel, "Text saved in: " + file.getPath());
            }
        });
        frame.setContentPane(myPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
    }
    private void display() {
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setLocationByPlatform(true);
        frame.setLocation((int) ((sw - frame.getWidth()) / 2), (int) ((sh - frame.getHeight()) / 2));
        frame.setVisible(true);
    } 
    public static void main(final String... args) {
        new SimpleGUIWithFileChooser().display();
     }
}
