package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

public class ControllerImpl implements Controller {

    private String s;
    private List<String> list;

    public ControllerImpl() {
        this.s = null;
        this.list = new ArrayList<String>();
    }

    public final void setStringToPrint(final String s) {
        try {
            this.s = s;
            list.add(s);
            } catch (Exception E){
             System.out.println("INSERT A STRING PLEASE!");
            }
    }

    public final String getStringToPrint() {
        return this.s;
    }

    public final void printString() throws IllegalAccessException {
       if(this.s==null) {
           throw new IllegalAccessException("String is not initialized!");
       }else {
           System.out.println(s);
    }
    }
    public final List<String> getStringHistory() {
       return this.list;
    }
}
