package it.unibo.oop.lab.mvcio;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import it.unibo.oop.lab.simplegui.MiniGUI;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {
    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    private final JFrame frame = new JFrame("My first GUI!!");
    private Controller myContr = new Controller();
    
    public SimpleGUI() {
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final JPanel myPanel = new JPanel();
        final JTextArea ta = new JTextArea("Insert text here!");
        final JButton save = new JButton("Save");
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final Color col1 = new Color(219 , 219 , 61);
        final  Color col2 = new Color(60, 223, 201);
        ta.setBackground(col1);
        ta.setFont(new Font("Arial Black", Font.BOLD, 20));
        save.setBackground(col2);
        myPanel.setLayout(new BorderLayout());
        myPanel.add(ta, BorderLayout.CENTER);
        myPanel.add(save, BorderLayout.SOUTH);
        save.addActionListener(new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                myContr.writeOnFile(ta.getText());
            }
        });
        frame.setContentPane(myPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setLocationByPlatform(true);
        frame.setLocation((int) ((sw - frame.getWidth()) / 2), (int) ((sh - frame.getHeight()) / 2));
        frame.setVisible(true);
    }
    
    public static void main(final String... args) {
        new SimpleGUI();
     }
}
